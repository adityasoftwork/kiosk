import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class EventsService {
  
  public apiUrl = environment.ApiUrl;
  constructor(private http: HttpClient) {}

  getData() {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', 'http://localhost:4200/')
      .set('Authorization', 'Basic f2334ea2d33e95f6bf3d45f914e37a6b');
    // .set('Host', '');
    return this.http.get(this.apiUrl+'/dc872bed2f2f52a9/events', { headers: headers }).pipe(map((response:any) => {
      return response;
    }));
  }
  getLocation(clientId:String) {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', 'http://localhost:4200/')
      .set('Authorization', 'Basic f2334ea2d33e95f6bf3d45f914e37a6b');
    // .set('Host', '');
    return this.http.get(this.apiUrl+'/locations/'+clientId, { headers: headers }).pipe(map((response:any) => {
      console.log("service respone"+JSON.stringify(response));
      
      return response;
    }));
  }
}
