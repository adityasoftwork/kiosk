import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder,  Validators,FormControl } from '@angular/forms';
import { EventsService } from '../shared/services/events.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
  private builder: FormBuilder,
  private clientService:EventsService
  ) { }
  public values:any=''; 
  public loginForm: FormGroup = new FormGroup({});
  public locations=[];
  public eventValue:any=[];

  ngOnInit(): void {
    this.clientForm();
  }
  
  clientForm(){
    this.loginForm = this.builder.group({
     
      clientId:new FormControl( '', [Validators.required, Validators.minLength(16),Validators.minLength(16)]),
      eventVanue: new FormControl('', [Validators.required])
    })
  }

  onKey(event: String)
  {  
    this.values=event;
    if(this.values.length>15)
    {
    this.clientService.getLocation(this.values).subscribe(data=>{
      this.locations=data;

      this.locations.map((response:any)=>{
         this.eventValue.push(response.name);
      })
    })
  }

    
  }
  checkUser()
  {
    console.log("fuction call");
    const payload = {
      clietId: this.loginForm.value.clietId,
      eventVanue: this.loginForm.value.eventVanue
    }
    if(this.loginForm.value.eventVanue && this.loginForm.value.clientId)
    {
      console.log("condition");
      this.router.navigateByUrl('/pairing/device');
    }
      
  }

}
