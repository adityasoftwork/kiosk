import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:'',loadChildren:()=>import('../app/home/home.module').then(m=>m.HomeModule)},
  { path:'home',loadChildren:()=>import('../app/home/home.module').then(m=>m.HomeModule)},
  { path:'app', loadChildren:()=>import('../app/layout/layout.module').then(m=>m.LayoutModule)},
  { path:'login', loadChildren:()=>import('../app/login/login.module').then(m=>m.LoginModule)},
  { path:'pairing', loadChildren:()=>import('../app/pairing/pairing.module').then(m=>m.PairingModule)},
  { path:'pyament-section',loadChildren:()=>import('../app/payment-section/payment-section.module').then(m=>m.PaymentSectionModule)},

  {
    path: '**',
    redirectTo: "404",
    pathMatch: "full"
  },
  // {
  //   path: '404',
  //   component: PageNotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
