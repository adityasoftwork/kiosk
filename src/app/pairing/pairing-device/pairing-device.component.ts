import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pairing-device',
  templateUrl: './pairing-device.component.html',
  styleUrls: ['./pairing-device.component.css']
})
export class PairingDeviceComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  
  pairing()
  {
    
    this.router.navigateByUrl('/pairing/res');
  }
  checkUser()
  {
    
  }


}
