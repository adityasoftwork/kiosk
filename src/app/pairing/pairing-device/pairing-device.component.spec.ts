import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PairingDeviceComponent } from './pairing-device.component';

describe('PairingDeviceComponent', () => {
  let component: PairingDeviceComponent;
  let fixture: ComponentFixture<PairingDeviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PairingDeviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PairingDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
