import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PairingRoutingModule } from './pairing-routing.module';
import { PairingComponent } from './pairing.component';
import { PairingResponsePageComponent } from './pairing-response-page/pairing-response-page.component';


@NgModule({
  declarations: [
    PairingComponent,
    PairingResponsePageComponent
  ],
  imports: [
    CommonModule,
    PairingRoutingModule
  ]
})
export class PairingModule { }
