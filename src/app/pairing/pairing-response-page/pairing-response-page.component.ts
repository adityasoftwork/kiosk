import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pairing-response-page',
  templateUrl: './pairing-response-page.component.html',
  styleUrls: ['./pairing-response-page.component.scss']
})
export class PairingResponsePageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  goToEvents()
  {
    console.log("goto event ");
    this.router.navigateByUrl('/app/enventChart');
  }

}
