import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PairingResponsePageComponent } from './pairing-response-page.component';

describe('PairingResponsePageComponent', () => {
  let component: PairingResponsePageComponent;
  let fixture: ComponentFixture<PairingResponsePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PairingResponsePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PairingResponsePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
