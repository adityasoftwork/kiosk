import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PairingDeviceComponent } from './pairing-device/pairing-device.component';
import { PairingResponsePageComponent } from './pairing-response-page/pairing-response-page.component';
import { PairingComponent } from './pairing.component';

const routes: Routes = [
  { path:'',component:PairingComponent,
    children:[
    {  path:'res',component:PairingResponsePageComponent },
    { path:"device", component:PairingDeviceComponent}
    
  ]
},
{
  path: '**',
  redirectTo: "/home",
  pathMatch: "full"
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PairingRoutingModule { }
