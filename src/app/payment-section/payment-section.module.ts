import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentSectionRoutingModule } from './payment-section-routing.module';
import { ConfirmationReceiptComponent } from './confirmation-receipt/confirmation-receipt.component';
import { PaymentReceipComponent } from './payment-receip/payment-receip.component';


@NgModule({
  declarations: [
    ConfirmationReceiptComponent,
    PaymentReceipComponent
  ],
  imports: [
    CommonModule,
    PaymentSectionRoutingModule
  ]
})
export class PaymentSectionModule { }
