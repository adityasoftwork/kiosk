import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationReceiptComponent } from './confirmation-receipt.component';

describe('ConfirmationReceiptComponent', () => {
  let component: ConfirmationReceiptComponent;
  let fixture: ComponentFixture<ConfirmationReceiptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmationReceiptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
