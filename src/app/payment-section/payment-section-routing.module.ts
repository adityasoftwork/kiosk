import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationReceiptComponent } from './confirmation-receipt/confirmation-receipt.component';
import { PaymentReceipComponent } from './payment-receip/payment-receip.component';
import { PaymentSectionComponent } from './payment-section.component';

const routes: Routes = [
  { path:'', component:PaymentSectionComponent,
   children:[
      { path:'paymentReceipt',component:PaymentReceipComponent},
      { path:'confirmation',component:ConfirmationReceiptComponent}
   ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentSectionRoutingModule { }
