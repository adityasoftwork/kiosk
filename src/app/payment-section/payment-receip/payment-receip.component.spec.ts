import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentReceipComponent } from './payment-receip.component';

describe('PaymentReceipComponent', () => {
  let component: PaymentReceipComponent;
  let fixture: ComponentFixture<PaymentReceipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentReceipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentReceipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
