import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowDetailsRoutingModule } from './show-details-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowDetailsRoutingModule
  ]
})
export class ShowDetailsModule { }
