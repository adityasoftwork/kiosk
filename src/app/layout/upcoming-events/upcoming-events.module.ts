import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpcomingEventsRoutingModule } from './upcoming-events-routing.module';
import { ShowDetailsComponent } from './show-details/show-details.component';
import { SittingArrangementsComponent } from './sitting-arrangements/sitting-arrangements.component';


@NgModule({
  declarations: [
    ShowDetailsComponent,
    SittingArrangementsComponent
  ],
  imports: [
    CommonModule,
    UpcomingEventsRoutingModule
  ]
})
export class UpcomingEventsModule { }
