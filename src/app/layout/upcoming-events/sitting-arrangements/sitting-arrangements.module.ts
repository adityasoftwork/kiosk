import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SittingArrangementsRoutingModule } from './sitting-arrangements-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SittingArrangementsRoutingModule
  ]
})
export class SittingArrangementsModule { }
