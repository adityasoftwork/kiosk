import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SittingArrangementsComponent } from './sitting-arrangements.component';

describe('SittingArrangementsComponent', () => {
  let component: SittingArrangementsComponent;
  let fixture: ComponentFixture<SittingArrangementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SittingArrangementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SittingArrangementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
