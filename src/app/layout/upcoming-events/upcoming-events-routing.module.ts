import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowDetailsComponent } from './show-details/show-details.component';
import { SittingArrangementsComponent } from './sitting-arrangements/sitting-arrangements.component';
import { UpcomingEventsComponent } from './upcoming-events.component';

const routes: Routes = [
  {
    path:'',component:UpcomingEventsComponent,
    children:[
      { path:'showdetails',component:ShowDetailsComponent },
      { path:'sittingarrangement',component:SittingArrangementsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpcomingEventsRoutingModule { }
