import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  { 
    path:'',component:LayoutComponent,
    children:[
      { path:'upcomingEvents', loadChildren:()=>import('./upcoming-events/upcoming-events.module').then(m=>m.UpcomingEventsModule)},
      { path:'enventChart', loadChildren:()=>import('./even-chart/even-chart.module').then(m=>m.EvenChartModule)},
      {
        path: '**',
        redirectTo: "404",
        pathMatch: "full"
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
