import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvenChartRoutingModule } from './even-chart-routing.module';
import { EventCardsComponent } from './event-cards/event-cards.component';
import { EventChartComponent } from './event-chart.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
  declarations: [
    EventCardsComponent,
    NavbarComponent,
    EventChartComponent
  ],
  imports: [
    CommonModule,
    EvenChartRoutingModule,
    HttpClientModule
  ],
  exports:[
    EventChartComponent
  ]

})
export class EvenChartModule { }
