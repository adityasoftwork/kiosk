import { Component, OnInit } from '@angular/core';
import { EventsService } from 'src/app/shared/services/events.service';

@Component({
  selector: 'app-event-chart',
  templateUrl: './event-chart.component.html',
  styleUrls: ['./event-chart.component.scss']
})
export class EventChartComponent implements OnInit {
 
  EventsData : any = [];
  constructor(private event : EventsService) {
   
   }

  ngOnInit(): void {
    this.getEventData();
  }

  getEventData()
  {
    this.event.getData().subscribe(data => {
      console.log(data);
      this.EventsData = data;
    })
  }

}
