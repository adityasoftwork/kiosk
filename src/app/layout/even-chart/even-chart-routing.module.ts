import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventCardsComponent } from './event-cards/event-cards.component';
import { EventChartComponent } from './event-chart.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [
  { path:'',component:EventChartComponent,
    children:[
      { path:"navbar",component:NavbarComponent },
      { path:"cards", component:EventCardsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvenChartRoutingModule { }
